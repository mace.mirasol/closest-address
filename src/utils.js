const axios = require('axios');
const url = 'https://maps.googleapis.com/maps/api/place/';
const autocomplete = 'autocomplete/json?input=';
const details = 'details/json?place_id=';
const apiKey = 'AIzaSyBDZjCJVC5mbasLkCXqVPBEFE06YSI0Eco';

// Retrieveing place_id/s from 'url/place/autocomplete'
const get_place_ids = async (province, municipality, barangay) => {
  try {
    const response = await axios.get(
      `${url}${autocomplete}${barangay}, ${municipality}, ${province}&types=geocode&key=${apiKey}`
    );

    const place_ids = [];
    response.data.predictions.forEach(place => place_ids.push(place.place_id));

    console.log('\nLocations retirieved:\n');
    response.data.predictions.forEach(place => console.log(place.description));

    if (place_ids.length === 0) console.log('No results found.\n');

    return place_ids;
  } catch (e) {
    console.log(e);
  }
};

// Retreiving lat and lng from 'url/place/details'
const get_coordinates = async place_ids => {
  try {
    const coordinates = [];

    console.log('\nCoordinates retirieved:\n');
    for (const place of place_ids) {
      const response = await axios.get(
        `${url}${details}${place}&fields=geometry&key=${apiKey}`
      );
      coordinates.push(response.data.result.geometry.location);
      console.log(
        `${response.data.result.geometry.location.lat}, ${response.data.result.geometry.location.lng} `
      );
    }

    if (coordinates.length === 0) console.log('No results found.\n');

    return coordinates;
  } catch (e) {
    console.log(e);
  }
};

// Retreiving and filtering suggestions based on coordinates
// from the previous function
const get_suggestions = async (coordinates, search_text) => {
  try {
    const suggestions = [];

    console.log(`\nPushing suggestions for '${search_text}' to array:\n`);
    for (const coordinate of coordinates) {
      const response = await axios.get(
        `${url}${autocomplete}${search_text}&location=${coordinate.lat},${coordinate.lng}&radius=10000&strictbounds&key=${apiKey}`
      );

      // PUSHING into suggestions array and FILTERING duplicate objects
      response.data.predictions.forEach(prediction => {
        if (
          suggestions.some(
            suggestion => suggestion.place_id === prediction.place_id
          )
        ) {
          console.log(`${prediction.description} already exists.`);
        } else {
          suggestions.push(prediction);
          console.log(`${prediction.description} pushed into array.`);
        }
      });
    }

    if (suggestions.length === 0) console.log('No results found.\n');

    return suggestions;
  } catch (e) {
    console.log(e);
  }
};

const filter_suggestions = (suggestions, province) => {
  const cleanedSuggestions = suggestions.map(suggestion => {
    // Looking for index of province
    let index = 0;

    for (let i = 0; i < suggestion.terms.length; i++) {
      if (suggestion.terms[i].value === province) index = i;
    }

    // Returning cleaned up object according to requirements
    return {
      address: suggestion?.description,
      barangay: suggestion?.terms[index - 2]?.value,
      municipality: suggestion?.terms[index - 1]?.value,
      province: suggestion?.terms[index]?.value,
    };
  });

  return cleanedSuggestions;
};

module.exports = {
  get_place_ids,
  get_coordinates,
  get_suggestions,
  filter_suggestions,
};
