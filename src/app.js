const express = require('express');
const {
  get_place_ids,
  get_coordinates,
  get_suggestions,
  filter_suggestions,
} = require('./utils');

const app = express();
const port = 3000;

app.get('/searchLocation', async (req, res) => {
  try {
    // Storing results of get_place_ids to use for coordinates
    const place_ids = await get_place_ids(
      req.query.province,
      req.query.municipality,
      req.query.barangay
    );

    // Storing results of coordinates to use for suggestions
    const coordinates = await get_coordinates(place_ids);

    // Storing results of suggestions to use for cleanedSuggestions
    const suggestions = await get_suggestions(
      coordinates,
      req.query.searchText
    );

    // Final results are stored in this array
    const filtered_suggestions = await filter_suggestions(
      suggestions,
      req.query.province
    );

    // Uncomment the code below and comment out -
    // the conditional statement at the end of the try block -
    // if you wish to see the unfiltered suggestions

    // res.send(suggestions);

    suggestions.length === 0
      ? res.send('No results found. Please check your input.')
      : res.send(filtered_suggestions);
  } catch (e) {
    console.log(e);
  }
});

app.listen(port, () => console.log('listening ' + port));
